@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('users.store') }}" method="post"  enctype="multipart/form-data">
        @csrf

        <label for="name">User Name</label>
        <input type="text" name="name"><br><br>
        <label for="email">Email</label>
        <input type="text" name="email"><br><br>
        <label for="phone_no">Phone No</label>
        <input type="text" name="phone_no"><br><br>
        <label for="date_of_birth">Date of Birth</label>
        <input type="date" name="date_of_birth"> <br><br>
        <label for="gender">Gender</label><br><br>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br><br>
        <label for="role_id">Register As</label>
        <select name="role_id" id="role_id" class="form-select">
            @foreach ($roles as $key=>$value)
            <option value="{{ $key }}">{{ $value }}</option>
            @endforeach
        </select><br><br>
        <label for="password">Password</label>
        <input type="text" name="password">
        <br>

        <button type="submit">Save</button>
    </form>
