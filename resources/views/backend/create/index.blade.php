@if (\Session::has('msg'))
    <div class="alert alert-primary" role="alert">
        {!! \Session::get('msg') !!}
    </div>
    @endif
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Users
            <table style="width: 100%; border-collapse: collapse" border="1">
                <thead>
                    <tr>
                        <th colspan="5">Registered user Table</th>
                        <th>
                            <form action="{{route('users.index')}}" method="get">
                                <input type="text" for='search' name='search' placeholder="Search">
                                {{-- <i class="ph-magnifying-glass opacity-50" aria-hidden="true"></i> --}}
                                <button type="submit"><i class="fas fa-search"></i></button>
                            </form>
                        </th>
                        <th><button><a href="{{route('users.create')}}">Add User</a></button></th>
                    </tr>
                    <tr>
                        <th>Ser No</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone No</th>
                        <th>Date of Birth</th>
                        <th>Gender</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $sl = 1
                    @endphp
                    @foreach ($users as $key => $user)
                    <tr>
                        <td>{{$sl++}}</td>
                        <td>{{$user->user_name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->phone_no}}</td>
                        <td>{{$user->date_of_birth}}</td>
                        <td>{{$user->gender}}</td>
                        <td>{{$user->role->role_name}}</td>
                        <td>
                            <button><a href="{{route('users.show', $user->id)}}">Show</a></button>
                            <button><a href="{{route('users.edit', $user->id)}}">Edit</a></button>
                            <form action="{{route('users.delete', $user->id)}}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{-- <div class="card-footer">
            {{$students->links()}}
        </div> --}}
    </div>
