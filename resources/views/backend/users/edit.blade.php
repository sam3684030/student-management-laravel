<form action="{{route('users.update', $user->id)}}" method="post">
    @csrf
    @method('PATCH')
    <label for="name">User Name</label>
    <input type="text" name="name" value="{{$user->name}}"><br>
    <label for="email">Email</label>
    <input type="text" name="email" value="{{$user->email}}"><br>
    <label for="phone_no">Phone No</label>
    <input type="text" name="phone_no" value="{{$user->phone_no}}"><br>
    <label for="date_of_birth">Date of Birth</label>
    <input type="date" name="date_of_birth" value="{{$user->date_of_birth}}"> <br>
    <label for="gender">Gender</label><br>
    <input type="radio" id="male" name="gender" value="male" @if($user->gender === "male") checked @endif>
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="gender" value="female" @if($user->gender === "female") checked @endif>
    <label for="female">Female</label><br>
    <label for="role_id">Register As</label>
        <select name="role_id" id="role_id" class="form-select">
            @foreach ($roles as $key=>$value)
            <option value="{{ $key }}">{{ $value }}</option>
            @endforeach
        </select><br>
    <label for="password">Password</label>
    <input type="text" name="password" value="{{$user->password}}"><br>
    <button type="submit">Update</button>
</form>
