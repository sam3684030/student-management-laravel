<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;

class UserController extends Controller
{
    public function index(Request $request)
    {
        // dd($request->search);
        $usersCollection = User::latest();
        if($request->search){
            $serach = $request->search;
            $usersCollection = User::where('name','LIKE','%'.$serach.'%')->latest()
                                            ->orWhere('email','LIKE','%'.$serach.'%');
        }
        $users =  $usersCollection->paginate(10);
        return view('backend.users.index', compact('users'));
    }
    public function create()
    {
        $roles = Role::orderBy('role_name', 'asc')
            ->pluck('role_name', 'id')
            ->toArray();

        return view('backend.users.create', compact('roles'));
    }
    public function store(Request $request)
    {
        try{
            $request->validate([
                'name' => 'required|min:5',
                'email' => 'required',
                'phone_no' => 'required',
                'date_of_birth' => 'required',
                'gender' => 'required',
                'password'=>'required|min:6',
            ]);
            User::create([
                'user_name' => $request->name,
                'email' => $request->email,
                'phone_no' => $request->phone_no,
                'date_of_birth' => $request->date_of_birth,
                'gender' => $request->gender,
                'role_id' => $request->role_id,
                'password'=>$request->password,
            ]);
            return redirect()->route('users.index')->with('msg', 'Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }


    }

    // public function uploadImage($file)
    // {
    //     // dd($file);
    //     if($file)
    //     {
    //         $file_name = time().'.'.$file->getClientOriginalExtension();
    //         Image::make($file)->save(public_path().'/images'.$file_name);
    //         // Image::make(storage_path().'/app/public/images'.$file_name);
    //         return $file_name;
    //     }
    // }

    public function show(User $user)
    {
        // $student = Student::where('id', $id)->first();
        return view('backend.users.show', compact('user'));
    }

    public function edit(User $user)
    {
        // $student = Student::where('id', $id)->first();
        return view('backend.students.edit', compact('user'));
    }
    public function update(Request $request, User $user)
    {
        // $student = Student::where('id', $id)->first();
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'phone_no' => $request->phone_no,
            'date_of_birth' => $request->date_of_birth,
            'gender' => $request->gender,

        ]);
        return redirect()->route('users.index');
    }


    public function destroy($id)
    {
        $user = User::where('id', $id)->first();
        $user->delete();
        return redirect()->route('users.index');
    }

}
